// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', 
	[
		'ionic',
		'Service.Socket',
		'Menu.Ctrl',
		'develop.Ctrl',
		'AllNotices.Ctrl'
	])

.run(function($ionicPlatform, Server) {
  $ionicPlatform.ready(function() {
	  
	  
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
	
	myLog('run ' + Date.now());
	myLog('platform ' + getPlatform());
	  
	//myLog('SessionID',sessID());

	lGoogle.Init();
	lFB.Init();
	lGoogle.CheckUser();
    //lGoogle.Login();
  });
})

.config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider
	.state('welcome', {
		url: '/',
		templateUrl: 'templates/welcome_page.html',
		controller: 'WelcomeCtrl'		
	})
	.state('app', {
		url: '/app',
		abstract: true,
		templateUrl: 'templates/menu.html',
		controller: 'MenuCtrl'
	})
	.state('app.allnotices', {
		url: '/allnotices',
		views: {
			'menuContent': {
				templateUrl: 'templates/allnotices.html',
				controller: 'AllnoticesCtrl'
			}
		}
	})
	.state('app.develop', {
		url: '/develop',
		views: {
				'menuContent': {
					templateUrl: 'templates/develop.html',
					controller: 'DevelopCtrl'
				}
			}
	})
	
	
	$urlRouterProvider.otherwise('/');
})



	

.controller('WelcomeCtrl', function ($scope, $state/*, $q, $ionicLoading*/, Server) {
	myLog('WelcomeCtrl');
	gl_Server = Server;
	
	$scope.Login_Google = function(){
		lGoogle.Login();
	}
	
	$scope.LogOut_Google = function(){
		lGoogle.Logout();
	}
	
	AllEvents.autor_notify_registr(function(){
		myLog('auth ok, goto all notices');
		$state.go('app.allnotices');
	});
	
	
	$scope.MGLogOff = function()
	{
		Server.logoff();
	}
	
	
	
	// Check for the various File API support.
	if (window.File && window.FileReader && window.FileList && window.Blob) {
	  // Great success! All the File APIs are supported.
	} else {
	  alert('The File APIs are not fully supported in this browser.');
	}


	
})
