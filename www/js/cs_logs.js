var LogMessages = [];

function myLog(text, obj) {
	var time = new Date().getTime();
	LogMessages.push({
		text: text,
		time: LogMessages.length + 1,
		object: obj
	});

	if (obj)
		console.log(text+'<br>', obj);
	else
		console.log(text);
}
