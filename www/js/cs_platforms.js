var SupportMobilePlatform=["android",'IOS'];
var SupportStandAlonePlatform=["win32",'linux','macintel'];

function IsLocalHost()
{
	if (window.location.hostname.indexOf('localhost') == -1)
		return false;
	else
		return true;
}

function IsStandAlone()
{
	return (SupportStandAlonePlatform.indexOf(getPlatform())>=0)?true:false;
}

function IsWindows()
{
	return getPlatform()=="win32"?true:false;
}
function IsMobile()
{
	return (SupportMobilePlatform.indexOf(getPlatform())>=0)?true:false;
}
function IsAndroid()
{
	return getPlatform()=="android"?false:true;
}
function IsIOS()
{
	return getPlatform()=="IOS"?false:true;
}

function getPlatform()
{
	return ionic.Platform.platform();
}