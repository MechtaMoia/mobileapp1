
/******************************************************************************************************************************************************************
*
******************************************************************************************************************************************************************/
angular.module('AllNotices.Ctrl', [ ]) 
.controller('AllnoticesCtrl', function($scope,$http, $ionicPopover) {
	$scope.allnotices = g_notices;
	$scope.User = CurSess;
	$scope.FilesToUpload = FilesToUpload;

	
	AllNotice_NewTopicInit();
	angular.element(document).ready(function () {
		//if (Auth.Mechta_Auth==true)
		//	$('.numberCircle2').css('background','green');		
	});
	
	UpdateFiles = function(){
		$scope.$apply();
	}		

	$scope.PlaceTopic = function ()
	{
		gl_Server.PlaceTopic($('#NewTopicPrice').val(),$('#NewTopicDesc').val());
	};
	
	$scope.delete_file = function(file){
		FilesToUpload.Files.forEach(function(item,i,arr){
			if (item.guid==file.guid){
				FilesToUpload.Files.splice(i,1);
			}
		});
	}
	
	AllEvents.new_topic_notify_registr(function(){
		$scope.allnotices = g_notices;
		$scope.$apply();
	});
	

	$scope.IsMy  = function(notice) {
		if (notice.User.Id==CurSess.MGUser.Id)
			return true;
		else
			return false;
	}

	
	//	показывать это объявление или нет
	$scope.isFilter = function(notice)
	{		
		var checked = $('#check_onlymy')[0].checked;
		if ( checked == false)	// значит все показываем
			return true;
		
		if (notice.User.Id==CurSess.MGUser.Id)
			return true;

		return false;
	};
  
		
	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////
	function AllNotice_NewTopicInit()
	{
		$scope.popover = $ionicPopover.fromTemplateUrl('templates/allnotices_newtopic.html', {
			scope: $scope
		}).then(function(popover) {
			$scope.popover = popover;
		});   

		$scope.openPopover = function($event) {
			$scope.popover.show($event);
			
			myLog('openPopover');
			
		};

		$scope.closePopover = function() {
			$scope.popover.hide();
		};

		$scope.$on('$destroy', function() {
			$scope.popover.remove();
		});

		$scope.$on('popover.hidden', function() {
			// Execute action
		});

		$scope.$on('popover.removed', function() {
			// Execute action
		});
	}
	
	
	myLog('allnoice controller');
	
  $scope.newTopicPreview = function (evt) {
  	myLog('fileload');
  	var files = evt.target.files; // FileList object

  	// files is a FileList of File objects. List some properties.
  	var output = [];
  	for (var i = 0, f; f = files[i]; i++) {
  		output.push('<li><strong>', escape(f.name), '</strong> (', f.type || 'n/a', ') - ',
  			f.size, ' bytes, last modified: ',
  			f.lastModifiedDate.toLocaleDateString(), '</li>');
  	}
  	document.getElementById('list').innerHTML = '<ul>' + output.join('') + '</ul>';
  }
  //document.getElementById('files').addEventListener('change', handleFileSelect, false);
		
})
.controller('AllnoticesNewTopicCtrl', function($scope,$http, Socket) {

	
})
;