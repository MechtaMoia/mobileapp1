/*

	1. при запуске приложения коннектимся к серверу,  передавая текущую куку.
	2. Connected
		a. проверяем локально google
		b. проверяем локально fb
		если a | b и d initResponse не пришел пользователь передаем авторизацию
		
		???? пока не делаю c. запускаем асинхронно проверку google и fb
		d. показываем страницу Welcome_Page
		
event:  если пришла успешная авторизация от социалки, от любой, то посылаем auth. Паша обрати внимание, что тебе гипотетически может прийти обе социалки
event:	если в какой то момент пришла успешная авторизация от сервера, все бросаем и идем работать на основную страницу


B0:50:CE:68:9E:C5:89:99:A9:7E:A0:C6:10:49:97:F2:D5:00:C4:CE
com.ionicframework.app1915887


308800775281-77oc4ctqdrmnoorkfs3k7bg1j58vjrpn.apps.googleusercontent.com

android
cordova plugin add cordova-plugin-googleplus --save --variable REVERSED_CLIENT_ID=308800775281-77oc4ctqdrmnoorkfs3k7bg1j58vjrpn.apps.googleusercontent.com
web
cordova plugin add cordova-plugin-googleplus --save --variable REVERSED_CLIENT_ID=308800775281-hom66gctj2vt5seidtsi0p537rrjbm5r.apps.googleusercontent.com
cordova plugin remove cordova-plugin-googleplus


https://accounts.google.com/o/oauth2/v2/auth?
 client_id=308800775281-77oc4ctqdrmnoorkfs3k7bg1j58vjrpn.apps.googleusercontent.com&
 response_type=code&
 scope=openid%20email&
 redirect_uri=https://oauth2-login-demo.example.com/code&
 state=security_token%3D138r5719ru3e1%26url%3Dhttps://oauth2-login-demo.example.com/myHome&
 login_hint=jsmith@example.com&
 openid.realm=example.com&
 hd=example.com



*/

angular.module('Service.Socket', [
	'btford.socket-io'
	])
///////////////////////////////////////////////////////////////
.factory('Socket', function (socketFactory) {
	var myIoSocket = io.connect('https://dev.marketgate.ru/');
	mySocket = socketFactory({
			ioSocket: myIoSocket
		});
	myLog('SocketIO created');
	gl_Server = mySocket;
	return mySocket;
})
///////////////////////////////////////////////////////////////
.factory('Server', function (Socket) {
	var messages = [];	
	myLog("SocketIO started with SessID",sessID());
	
	var connected = false;
	var wait_cmd = [];
	var uploader = new SocketIOFileUpload(Socket);
	
  uploader.addEventListener("start", function(event){
        // закачка картинки для нового топика //
	  	alert('uploader.addEventListener');
        if(currentCreatingTopicId!=null){
            event.file.meta.data = {TopicId: currentCreatingTopicId};  // Id топика, для которого картинка //
        }
    });	
	
	/******************************************/
	//	что бы не усложнять логику поведения так или иначе, пусть всегда данные складываются, а потом при подключении отошлются
	function pushAllWaitCmd(){
		wait_cmd.forEach(function(item,i,arr){
			myLog(item,i);
			send(item.cmd,item.obj);
		});
	}	
	function send(cmd, obj)
	{
		//if (connected==true){
			//	где это надо?
			myLog("send:"+cmd,obj);
			Socket.emit(cmd,obj);
		/*}else{
			myLog("add wait cmd:"+cmd,obj);
			wait_cmd.push({cmd,obj});
		}*/
	}
	/******************************************/
	
	////////////////////////////////////
	Socket.on('connect', function (msg) {
		myLog('connected',msg);
		connected = true;
		send('init', {Version: AppVersion,  SessionId:sessID()});
	});
	
	////////////////////////////////////
	Socket.on('initResponse', function (msg) {
		myLog('initResponse',msg);
		
		if(msg["SessionId"]!=null)
			setCookie("SessionId",msg["SessionId"]);
		
		if (msg.User){
			CurSess.MGUser = msg.User;
			AllEvents.autor_notify_event();	//	нужно для автоматического перехода с велком пейдж
			
		}else{
			if (lGoogle.CheckUser()){
				auth(lGoogle.System(), lGoogle.Token());
			}else if(lFB.CheckUser()){
				auth(lFB.System(), lFB.Token());
			}
		}			

		//	никакого разделения пока по регионам и авторизированности пользователя нет. Поэтой причине можем сразу получить все топики, потом просто их подстветим
		send('getTopics', {location: 1});
	});
	
	////////////////////////////////////
	Socket.on('getTopicsResponse', function (msg) {
		myLog('getTopicsResponse',msg);

		var arr = msg.Topics;
		arr.forEach(function (item, i, arr) {
			g_notices.push(item);
		});
		//	here you can sort or other processing
		//if (event_loadtopics_to_notice)
		//	event_loadtopics_to_notice();
	}); /**/
	
	////////////////////////////////////
	Socket.on('eventTopicNew', function (msg) {
		myLog('eventTopicNew',msg);
		g_notices.unshift(msg);

		//g_notices[msg.TopicId] = msg;
		//AllEvents.new_topic_notify_event();
		//if (event_loadtopics_to_notice)
		//	event_loadtopics_to_notice();
		//	here you can sort or other processing
	});
	
	////////////////////////////////////
	Socket.on('getSession', function (msg) {
		console.log('getSession');
		send('sendSession', getCookie('myTestCookie'));
	});
	
	var lastTopic;
	////////////////////////////////////
	Socket.on('getChatResponse', function (msg) {
		myLog('getChatResponse for last topic:'+lastTopic,msg);
		
		var messages = getChat(lastTopic).msg;
		var arr = msg.Messages;
		arr.forEach(function (item, i, arr) {
			ChatMessages[lastTopic].msg.push(item);
		});
		
		if (event_loadchat_to_notice)
			event_loadchat_to_notice();
		
	});
	
	Socket.on('eventChatMsgNew',function(msg){
		myLog('eventChatMsgNew',msg);
		ChatMessages[msg.TopicId].msg.unshift(msg);
		if (event_loadchat_to_notice)
			event_loadchat_to_notice();
	});

	////////////////////////////////////
	Socket.on('createTopicResponse', function (msg) {
		myLog('createTopicResponse',msg);
		if (!msg.TopicId) {
			$('#NewTopicHead').text(msg.Text);
			$('#NewTopicHead').attr("style", "color:red");
		}
	});

	////////////////////////////////////
	Socket.on('authResponse', function (msg) {
		myLog('authResponse',msg);
		
		if (msg.User){
			CurSess.MGUser = msg.User;
			AllEvents.autor_notify_event();	//	нужно для автоматического перехода с велком пейдж
		}

		if (!msg.TopicId) {
			//$('#NewTopicHead').text(msg.Text);
			//$('#NewTopicHead').attr("style","color:red");
		}
	});

	////////////////////////////////////
	Socket.on('disconnect', function (msg) {
		myLog('disconnect',msg);
		connected=false;
	});

	////////////////////////////////////
	Socket.on('reconnect', function (msg) {
		myLog('reconnect',msg);
	});

	////////////////////////////////////
	Socket.on('reconnecting', function (attempt) {
		myLog('reconnecting',attempt);
	});

	////////////////////////////////////
	function auth(System, Token) {
		var packet = {
			AuthSystem: System,
			AccessToken  : Token,
			appId : lGoogle.getWebClientID()
		}		
		send('auth', packet);
	}
	
	////////////////////////////////////
	////////////////////////////////////
	////////////////////////////////////
	return {
		getMessages: function () {
			return messages;
		},
		getChat: function (TopicId) {
			lastTopic = TopicId;	
			send('getChat', {TopicId: TopicId});
		},
		readChatMsg:function(){
			
		},
		placeChat: function(TopicId,Text)
		{			
			send('createChatMsg',{TopicId:TopicId, Text: Text});
		},
		PlaceTopic: function (Price, Text) {
			send('createTopic', {
				Price: Price,
				Text: Text
			});
		},
		auth: function (System, Token) {
			auth(System, Token);
		},
		logoff: function(){
			send('logoff',{});
		}
	};
})