var lGoogle = getGoogle();


function httpGet(theUrl)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", theUrl, false ); // false for synchronous request
    xmlHttp.send( null );
    return xmlHttp.responseText;
}

function getGoogle()
{	
	var lvInfo ={
            idToken:"",
            displayName:"",
            email:""
        };

    function lf_getWebClientID(){
        
        if (IsMobile())return '308800775281-hom66gctj2vt5seidtsi0p537rrjbm5r.apps.googleusercontent.com';
		if (IsStandAlone() && IsLocalHost())  return '63192690304-h7gpb8k8f76hg7jrj06p4kdmfucr2eff.apps.googleusercontent.com';
		if (IsStandAlone() && !IsLocalHost())  return '63192690304-k6fg77s1nedlqdmui2kbmli4f30nbrtj.apps.googleusercontent.com';		
    }
    
	var auth2=null;
	return {
		Auth2           : function(){return auth2;},
        Info            : function(){return lvInfo;},
        getWebClientID  : function(){return lf_getWebClientID();},
		Init: function(){
			if (IsMobile()){
				//app.initialize();				
			}
			if (IsStandAlone()){

				gapi.load('auth2', function() {
                    myLog('google init. webClientId',lf_getWebClientID());
					auth2 = gapi.auth2.init( { client_id:	lf_getWebClientID()});
					myLog('windows GApi init',auth2);
					
					/*var x = auth2.isSignedIn;
					myLog('Is',x);*/
					
					gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);
					
					function updateSigninStatus(msg){
						myLog('updateSigninStatus',msg);
						//lGoogle.Login();
					}
					
				 });
			
			
				
			}
		},
		CheckUser: function(){
			if (IsMobile()){
				window.plugins.googleplus.trySilentLogin(
					{scopes:'idToken email givenName'},
					function (obj) {
						myLog('G CheckUser succ',obj);
						alert('check ok '+  "Silent hi, " + obj.displayName + ", " + obj.email,obj);
                                                alert(JSON.stringify(obj));
					},
					function (msg) {
					  //alert('check1 err'+msg);
					}
				);
			}
			
			if (IsStandAlone())
			{
				
			}
			
		},		
		Login: function(){
			myLog('google.Login');
			if (IsMobile()){
				myLog('google.Login.mobile');
				window.plugins.googleplus.login(
                                {
                                 'scopes':'https://www.googleapis.com/auth/contacts.readonly profile'
                                 , 'webClientId' :lf_getWebClientID()
                                 ,'offline': true
                                 },
					function (obj) {
						myLog('G Login succ',obj);
                                                alert(JSON.stringify(obj));

                                                //obj.idToken;
                                                gl_Server.auth(lGoogle.System(),obj.idToken);



				},
					function (msg) {
						alert('check2 err'+msg);

				});
			}
			if (IsStandAlone()){
				auth2.grantOfflineAccess().then(signInCallback);
				function signInCallback(x){
					myLog('signInCallback',x);
					gl_Server.auth(lGoogle.System(),x.code);
				}
			}
		},
		Logout: function(){
			window.plugins.googleplus.logout(
				function (msg) {
					alert('logout ok',msg);
			},
				function (msg) {
					alert('logout err',msg);
			});
		},
		Tocken: function(){return "";},
		System: function(){
            if (IsMobile())
                return "google";
            else 
                return "google_web";
        } ,
        getCert: function(){            
                myLog('google.getCert');
                if (IsMobile()){
                         window.plugins.googleplus.getSigningCertificateFingerprint(
				                function (msg) {    myLog('cert received',msg); },
				                function (msg) {	myLog('googleplus.getSigningCertificateFingerprint err',msg);},
                               "GooglePlus", "getSigningCertificateFingerprint", []
                        );
                }
        }
	}
}



function onSignIn(googleUser) {
  var profile = googleUser.getBasicProfile();
  alert('google ok');
  console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
  console.log('Name: ' + profile.getName());
  console.log('Image URL: ' + profile.getImageUrl());
  console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
}